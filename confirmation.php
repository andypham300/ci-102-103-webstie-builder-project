<?php
require_once 'header.php';
?>


<DOCTYPE html>
<html>
<head>
    <style>
        h1 {
            margin: 2rem;
            text-align: center;
            color: white;
        }
        .deleteButton {
            text-align: center;
            margin: 2rem;
        }

        .deleteButton .deleteYes{
            width: 50%;
            padding: 0.625rem;
            border-radius: 0.35rem;
            border: 0;
            margin-bottom: 0.35rem;
            outline: 0;
            background-color: darkred;
            color: white;
        }

        .deleteButton .deleteNo{
            width: 50%;
            padding: 0.625rem;
            border-radius: 0.35rem;
            border: 0;
            margin-bottom: 0.35rem;
            outline: 0;
            background-color: darkgreen;
            color: white;
        }

        .deleteButton .deleteYes:hover{
            opacity: 0.5;
            cursor: pointer;
        }

        .deleteButton .deleteNo:hover{
            opacity: 0.5;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <h1>Are you sure you want to delete your account?</h1>

    <form action="delete.inc.php" method="post" class="deleteButton">
    <input type="submit" name="Yes" value="Yes" class="deleteYes">
    </form>

    <form action="profile.php" method="post" class="deleteButton">
    <input type="submit" name="No" value="No" class="deleteNo">
    </form>
</body>
</hmtl>