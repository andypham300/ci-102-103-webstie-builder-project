
<?php 

require_once 'dbh.inc.php';

// sql to delete a record
$sql = "DELETE FROM users WHERE userID = userID";

if ($connection->query($sql) === TRUE) {
    require_once 'logout.inc.php';
} else {
    echo "Error deleting record: " . $connection->error;
}

$connection->close();

?>