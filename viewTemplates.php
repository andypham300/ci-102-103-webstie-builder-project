<?php
    include_once 'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script type="text/javascript" src="downloadTemplate.js"></script>
  </head>
<body>
  <div style = "width: 100%; height: 10vh; background-color: rgb(114, 153, 236)"></div>
  <p class="text-center mb-0" style = "background-color: rgb(114, 153, 236); color: white;">Check out our templates here!</p>
  <div class = "mb-3" style = "width: 100%; height: 10vh; background-color: rgb(114, 153, 236);"></div>

  <div class="d-flex flex-wrap justify-content-around w-50" style="margin: auto">
    <div class="w-25 me-1 downloadBtn">
      <button type="button" class="btn btn-primary w-100" onClick="downloadTemp()">Template 1</button> <!--Below the card should be an image of said layout; ideas: -->
      <img src="https://media.istockphoto.com/id/1335247217/vector/loading-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=jARr4Alv-d5U3bCa8eixuX2593e1rDiiWnvJLgHCkQM=" alt="Image 1" class="img-thumbnail mb-2 border ">
    </div>
    <div class="w-25 me-1">
      <button type="button" class="btn btn-primary w-100" onClick="downloadTemp()">Template 2</button>
      <img src="https://media.istockphoto.com/id/1335247217/vector/loading-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=jARr4Alv-d5U3bCa8eixuX2593e1rDiiWnvJLgHCkQM=" alt="Image 1" class="img-thumbnail mb-2 border ">
    </div>
    <div class="w-25 me-1">
      <button type="button" class="btn btn-primary w-100" onClick="downloadTemp()">Template 3</button>
      <img src="https://media.istockphoto.com/id/1335247217/vector/loading-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=jARr4Alv-d5U3bCa8eixuX2593e1rDiiWnvJLgHCkQM=" alt="Image 1" class="img-thumbnail mb-2 border ">
    </div>
    <div class="w-25 me-1">
      <button type="button" class="btn btn-primary w-100" onClick="downloadTemp()">Template 4</button> 
      <img src="https://media.istockphoto.com/id/1335247217/vector/loading-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=jARr4Alv-d5U3bCa8eixuX2593e1rDiiWnvJLgHCkQM=" alt="Image 1" class="img-thumbnail mb-2 border ">
    </div>
    <div class="w-25 me-1">
      <button type="button" class="btn btn-primary w-100" onClick="downloadTemp()">Template 5</button> 
      <img src="https://media.istockphoto.com/id/1335247217/vector/loading-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=jARr4Alv-d5U3bCa8eixuX2593e1rDiiWnvJLgHCkQM=" alt="Image 1" class="img-thumbnail mb-2 border ">
    </div>
    <div class="w-25 me-1">
      <button type="button" class="btn btn-primary w-100" onClick="downloadTemp()">Template 6</button>
      <img src="https://media.istockphoto.com/id/1335247217/vector/loading-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=jARr4Alv-d5U3bCa8eixuX2593e1rDiiWnvJLgHCkQM=" alt="Image 1" class="img-thumbnail mb-2 border ">
    </div>
    <!-- Add more buttons as needed -->
  </div>
  <div style = "width: 100%; height: 10vh; backgound-color: rgb(114, 153, 236);"></div>
  <div class="sticky-div" style="position: fixed; bottom: 0; width: 100%; height: 10vh; background-color: green; border: 2px solid #4bb44e;">
    <div class="sticky-div-content" style="display: flex; justify-content: center; align-items: flex-end; height: 100%; padding-bottom: 10px;">
      <p class="text-center text-white" style="width: 100%;">
        More Products&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Privacy Policy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Guides&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Help
      </p>
    </div>
  </div>
  
</body>
</html>