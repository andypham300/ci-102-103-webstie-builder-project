<?php
//If the user actually submitted the form the proper way, then run the code inside this file, if not send them back to the form
if (isset($_POST["submit"])) {
    $name = $_POST["name"];
    $email = $_POST["email"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $pwdrepeat = $_POST["pwdrepeat"];

    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';

    //if the user forgot to fill an input, tell the user that they forgot to fill an input(s)
    if (emptySignupInputs($name, $email, $username, $password, $pwdrepeat) !== false) {
        header("location: signup.php?error=emptyinput");
        exit();
    } 
    //If the user enters a username that contains a character that is not in the realm of a-z, A-Z, or 0-9, then the 'error=invalidusername' will pop up in the URL
    if (invalidUsername($username) !== false) {
        header("location: signup.php?error=invalidusername");
        exit();
    } 
    //If the user enters an invalid email (without the @.......), then the 'error=invalidemail' will pop up in the URL
    if (invalidEmail($email) !== false) {
        header("location: signup.php?error=invalidemail");
        exit();
    } 
    //If the user's passwords don't match, then the 'error=passwordsdontmatch' will pop up in the URL 
    if (pwdMatch($password, $pwdrepeat) !== false) {
        header("location: signup.php?error=passwordsdontpatch");
        exit();
    } 
    //If the user enters a username that already exists, then the 'error=usernametaken' will pop up in the URL
    if (usernameExists($connection, $username, $email) !== false) {
        header("location: signup.php?error=usernametaken");
        exit();
    } 
    //If all the fields follow the reuirements, then create the account
    createUser($connection, $name, $email, $username, $password);

}   //If at least one of the input fields doesn't follow their requirements, then reset the form
else {
    header("location: signup.php");
    exit();
}