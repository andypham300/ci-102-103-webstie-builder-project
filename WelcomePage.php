<?php
    include_once 'header.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E-dibles</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link href="WelcomePage.css" rel="stylesheet">
</head>
  <body style="background-color: rgb(13, 17, 23);">
    <!-- Temp Header -->
    <!--<header class="header p-3 position-absolute start-0 top-0 end-0" style="background-color: black;">
        <div class="d-flex justify-content-between align-items-center">
          <a href="#" class="text-decoration-none text-white fs-5 fw-bold">E-dibles</a>
        </div>-->
    </header>
    <div 
        class="masthead" 
        style="background-image: url('./images/oneliberty-lights-pinks.webp');"   
    >
        <div class="color-overlay d-flex align-items-center justify-content-center">
                <div class="row" align="center">
                    <?php
                        if (isset($_SESSION["userID"])) {
                            echo "<h1 style=text-align:center; color: white;>Hello there " . $_SESSION["username"] . "</h1>" ;    
                        }    
                    ?>
                    <h1> Our Designs, Your Solution </h1>
                    <div class="spacer1"></div>
                    <a href="#about" type="button" class="btn btn-outline-light"> Learn More </a>
                </div>
            </div>
        </div>

    </div>

    <div class="spacer">
    <section id="about"> 
        <div class="container">
            <div class="d-flex justify-content-center">
                <h1 style="color: white"> About Us </h1>
            </div>
            <div class="paragraph"> <p style="color: white"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-3"> <img src="https://st3.depositphotos.com/9998432/13335/v/450/depositphotos_133352010-stock-illustration-default-placeholder-man-and-woman.jpg" class="img-thumbnail"> <p style="color: white" > FirstName LastName</p> </div>
                <div class="col-3"> <img src="https://st3.depositphotos.com/9998432/13335/v/450/depositphotos_133352010-stock-illustration-default-placeholder-man-and-woman.jpg" class="img-thumbnail"> <p style="color: white" > FirstName LastName</p> </div>
                <div class="col-3"> <img src="https://st3.depositphotos.com/9998432/13335/v/450/depositphotos_133352010-stock-illustration-default-placeholder-man-and-woman.jpg" class="img-thumbnail"> <p style="color: white" > FirstName LastName</p> </div>
                <div class="col-3"> <img src="https://st3.depositphotos.com/9998432/13335/v/450/depositphotos_133352010-stock-illustration-default-placeholder-man-and-woman.jpg" class="img-thumbnail"> <p style="color: white" > FirstName LastName</p> </div>
            </div>
        </div>
    </section>
    </div>

    <div class="spacer">
    <section id="demo">
        <div class="container">
            <div class="d-flex justify-content-center">
                <div class="col-3"> <h1 style="color: white"> Our Product </h1> </div>
            </div>
            <div class="paragraph"> <p style="color: white"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> </div>
        </div>
    </section>
    </div>

    <div class="spacer1">
    <div class="container">
        <div class="row">
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" loading="lazy" class="hero__video">
            <source src="images/placeholdervid.mp4" type="video/mp4">
          </video>
        </div>
    </div>
    </div>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.7/dist/umd/popper.min.js" integrity="sha384-zYPOMqeu1DAVkHiLqWBUTcbYfZ8osu1Nd6Z89ify25QV9guujx43ITvfi12/QExE" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.min.js" integrity="sha384-Y4oOpwW3duJdCWv5ly8SCFYWqFDsfob/3GkgExXKV4idmbt98QcxXYs9UoXAB7BZ" crossorigin="anonymous"></script>
</body>
</html>