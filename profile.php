<?php
    include_once 'header.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <style>
            h1, h2, p{
                margin: 2rem;
                text-align: center;
            }

            .deleteAccount {
                text-align: center;
            }

            .deleteAccount input{
                text-align: center;
                width: 45%;
                padding: 0.625rem;
                border-radius: 0.35rem;
                border: 0;
                margin-bottom: 0.35rem;
                outline: 0;
                background-color: white;
                color: black;
            }

            .deleteAccount input:hover{
                opacity: 0.6;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <h1>Profile Page</h1>
            <p style="color: white;">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi tenetur ut eum est quasi pariatur quam 
                labore sequi atque rerum id amet quaerat, nulla ea reprehenderit distinctio omnis aliquid excepturi rem debitis necessitatibus. 
                Debitis explicabo voluptates atque accusamus beatae error labore recusandae alias pariatur! Minus id commodi labore maiores quos.
            </p>
            <h2 style="color: white;">Delete Account</h2>
            <form action="confirmation.php" method="post" class="deleteAccount">
                <div>
                    <input type="submit" name="accountDelete" value="Delete"></input>
                </div>
            </form>
    </body>
</html>
<?php


