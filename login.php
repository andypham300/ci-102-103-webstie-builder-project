<?php
    include_once 'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="form.css">
</head>
    <body>
        <section class="login-form">
            <h2>Login</h2>
            <div>
                <form action="login.inc.php" method="post">
                    <div class="input-ctn">
                        <input type="text" name="username" placeholder="Username/Email">
                    </div>
                    <div class="input-ctn">
                        <input type="password" name="password" placeholder="Password">
                    </div>    
                    <div>
                        <button type="submit" name="submit">Log In</button>
                    </div>
                </form>
            </div>
            <?php
                if (isset($_GET["error"])) {
                    if($_GET["error"] == "emptyinput") {
                        echo "<p>Fill in all fields!</p>";
                    }
                    else if ($_GET["error"] == "wronglogin") {
                        echo "<p>Incorrect Login Information!</p>";
                    }
                }
            ?>  
        </section>
    </body>
</html>
