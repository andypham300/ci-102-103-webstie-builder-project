<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="common.css">
</head>
<body>
    <nav class="navbar">
            <ul style = "width: 100%">
                <li><a href="WelcomePage.php">Home</a></li>
                <?php 
                //Once logged in, The sign up and log in page will disappear in the navigation bar, and the pages 'templates', 'profile page', and 'logout page' will pop up
                    if (isset($_SESSION["userID"])) {
                        echo "<li><a href='viewTemplates.php'>Templates</a></li>&nbsp;";
                        echo "<li><a href='profile.php'>Profile</a></li>&nbsp;";
                        echo "<li><a href='logout.inc.php'>Log Out</a></li>&nbsp;";
                    }
                    else {
                        echo "<li><a href='signup.php'>Sign Up</a></li>&nbsp;";
                        echo "<li><a href='login.php'>Login</a></li>";
                    }
                ?>
            </ul>
    </nav>
</body>
</html>