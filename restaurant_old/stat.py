import numpy as np

pi = np.pi
x = np.linspace(0, 2*pi, 127)  
plt.plot(x, np.sin(x))       
x = np.linspace(0, 2*pi, 127)  # evenly spaced intervals over [0,2pi]
plt.plot(x, np.sin(x))         # plot (x,y)
plt.show()