
function del(e)
{
    e.parentNode.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode.parentNode);
    console.log("Hello world!")
}

function create()
{
    var copy = document.querySelector(".cardContainer");
    var clone = copy.cloneNode(true)

    copy.after(clone);
}

function makeEditable(a) {
    var label = a;
    var input = document.createElement("input");
    input.type = "text";
    input.value = label.innerText;
    label.innerText = "";
    label.appendChild(input);
    input.focus();
    input.addEventListener("blur", function() {
      label.innerText = input.value;
    });
    
    input.addEventListener("keydown", function(event) {
      if (event.keyCode === 13) { // 13 is the code for "Enter" key
        label.innerText = input.value;
      }
    });
  }