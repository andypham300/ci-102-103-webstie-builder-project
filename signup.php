<?php
    include_once 'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="stylesheet" href="form.css">
</head>
    <section class="signup-form">
        <h2>Sign Up</h2>
        <div>
            <form action="signup.inc.php" method="post">
                <div class="input-ctn">
                    <input type="text" name="name" placeholder="Full Name">
                </div>
                <div class="input-ctn">
                    <input type="text" name="email" placeholder="Email">
                </div>
                <div class="input-ctn">
                    <input type="text" name="username" placeholder="Username">
                </div>
                <div class="input-ctn">
                    <input type="password" name="password" placeholder="Password">
                </div>    
                <div class="input-ctn">
                    <input type="password" name="pwdrepeat" placeholder="Repeat Password">
                </div>
                <div>
                    <button type="submit" name="submit">Sign up</button>
                </div>
            </form>
        </div>
        <br>
        <?php
        /*If the user tries to sign up with an input box being empty, then the 'error=emtpyinput' 
        in the URL will pop up from the 'signup.inc.php' as the code runs meaning that there is/are empty input boxes,
        the message 'Fill in all fields!' will show up in the bottom of the form. */ 
        if (isset($_GET["error"])) {
            if($_GET["error"] == "emptyinput") {
                echo "<p>Fill in all fields!</p>";
            }
            else if ($_GET["error"] == "invalidusername") {
            echo "<p>Choose a proper username!</p>";
            }
            else if ($_GET["error"] == "invalidemail") {
                echo "<p>Choose a proper email!</p>";
            }
            else if ($_GET["error"] == "passwordsdontmatch") {
                echo "<p>Passwords doesn't match!</p>";
            }
            else if ($_GET["error"] == "statementfailed") {
                echo "<p>Something went wrong, try again!</p>";
            }
            else if ($_GET["error"] == "usernametaken") {
                echo "<p>Username already taken!</p>";
            }
            else if ($_GET["error"] == "none") {
                echo "<p>You have signed up!</p>";
            }
        }
    ?>    
    </section>
    
